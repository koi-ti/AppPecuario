import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalNacimientoPage } from './animal-nacimiento.page';

describe('AnimalNacimientoPage', () => {
  let component: AnimalNacimientoPage;
  let fixture: ComponentFixture<AnimalNacimientoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimalNacimientoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalNacimientoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
