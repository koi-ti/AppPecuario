import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DatabaseService } from 'src/app/database.service';
import { Router } from '@angular/router';
import { AlertController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-animal-nacimiento',
  templateUrl: './animal-nacimiento.page.html',
  styleUrls: ['./animal-nacimiento.page.scss'],
})
export class AnimalNacimientoPage implements OnInit {


  ListEspecie: any;
  ListRaza: any;
  ListPadre: any;
  ListMadre: any;
  ListHato: any;
  ListLote: any;
  ListMarca: any;
  todo: FormGroup;
  items: Array<any>;
  sql: any;

  constructor(
    private database: DatabaseService,
    private router: Router,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private zone: NgZone,
    public actionSheetController: ActionSheetController
  ) {


    this.todo = this.formBuilder.group({
      animal_numero: ['', Validators.required],
      animal_especie: ['', Validators.required],
      animal_raza: ['', Validators.required],
      animal_color: [''],
      animal_macho: ['m'],
      animal_ingreso: ['', Validators.required],
      animal_peso_nacer: [''],
      animal_padre: ['', Validators.required],
      animal_madre: ['', Validators.required],
      animal_hato: [''],
      animal_lote: [''],
      animal_marca: [''],
      animal_rdfi: [''],
      // hato_activo: true,
      // animal_observaciones: [''],
      // animal_descripcion: [''],
      animal_activo: true,
      animal_observaciones: [''],
    });

  }

  ngOnInit() {
    this.GetEspecie();
    this.GetRaza();
    this.GetPadre();
    this.GetMadre();
    this.GetHato();
    this.GetLote();
    this.GetMarca();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      // header: 'El Hato ha sido creado con éxito',
      // subHeader: 'Subtitle',
      message: 'El animal ha sido registrado',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          handler: () => {

            this.goBack();
          }
        }]
    });

    await alert.present();
  }

  Create() {
    this.todo.value.animal_activo = true ? 1 : 0;
    this.todo.value.animal_hato = this.todo.value.animal_hato ? this.todo.value.animal_hato : null;
    this.todo.value.animal_lote = this.todo.value.animal_lote ? this.todo.value.animal_lote : null;
    this.todo.value.animal_marca = this.todo.value.animal_marca ? this.todo.value.animal_marca : null;
  
    this.sql = "INSERT INTO animals (animal_numero, animal_especie, animal_raza, animal_color, animal_macho, animal_ingreso, animal_peso_nacer, animal_padre, animal_madre, animal_hato, animal_lote, animal_marca, animal_rdfi, animal_activo, animal_observaciones) VALUES(?,?,?,?,?,datetime(?),?,?,?,?,?,?,?,?,?)";

    this.database.Createdatabase(this.todo.value, this.sql).then((data) => {
    }, (error) => {
      console.log(error);
    })
  }

  GetEspecie() {
    let sql = "SELECT * FROM especies WHERE especie_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListEspecie = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetRaza() {
    let sql = "SELECT * FROM razas WHERE raza_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListRaza = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetPadre() {
    let sql = "SELECT * FROM animals WHERE animal_macho='m'";
    this.database.Getdatabase(sql).then((data) => {
      this.ListPadre = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetMadre() {
    let sql = "SELECT * FROM animals WHERE animal_macho='f'";
    this.database.Getdatabase(sql).then((data) => {
      this.ListMadre = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetHato() {
    let sql = "SELECT * FROM hatos WHERE hato_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListHato = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetLote() {
    let sql = "SELECT * FROM lotes WHERE lote_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListLote = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetMarca() {
    let sql = "SELECT * FROM marcas WHERE marca_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListMarca = data;
    }, (error) => {
      console.log(error);
    })
  }

  Delete(id: number) {
    let sql = 'DELETE FROM animals WHERE id=?';
    this.database.Deletedatabase(id, sql).then((data) => {
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.zone.run(() => {
      this.router.navigateByUrl('/mostrar-animal');
    });
  }
}
