import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RazaPage } from './raza.page';

describe('RazaPage', () => {
  let component: RazaPage;
  let fixture: ComponentFixture<RazaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RazaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RazaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
