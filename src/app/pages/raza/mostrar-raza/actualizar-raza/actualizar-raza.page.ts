import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-actualizar-raza',
  templateUrl: './actualizar-raza.page.html',
  styleUrls: ['./actualizar-raza.page.scss'],
})
export class ActualizarRazaPage implements OnInit {

  ListEspecie: any;
  ListItemEspecie = {};
  edit: FormGroup;
  item: any;

  constructor(
    private database: DatabaseService,
    private router: Router,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private zone: NgZone,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.GetEspecie();
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM razas where id = ?";
        this.database.getItemById(data.id, sql).then(datax => {
          this.item = datax;
          if (!this.item) {
            this.goBack();
          }
          else {
            if (this.item.raza_activo == 1) { this.item.raza_activo = true }
            else { this.item.raza_activo = false }
            this.edit = this.formBuilder.group({
              raza_nombre: new FormControl(this.item.raza_nombre, Validators.required),
              raza_especie: new FormControl(this.item.raza_especie, Validators.required),
              raza_activo: new FormControl(this.item.raza_activo)
            });
            this.GetItemEspecie(this.item.raza_especie);
          }
        });
      }
    )
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Atención',
      // subHeader: 'Subtitle',
      message: 'El raza ha sido actualizado',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          cssClass: 'secondary',
          handler: () => {
            this.goBack();
          }
        }
      ]
    });

    await alert.present();
  }

  GetEspecie() {
    let sql = "SELECT * FROM especies WHERE especie_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListEspecie = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetItemEspecie(lote_hato) {
    let sql = "SELECT * FROM especies WHERE id=?";
    this.database.getItemById(lote_hato, sql).then((data) => {
      this.ListItemEspecie = data;
    }, (error) => {
      console.log(error);
    })
  }

  Update(edit) {
    if (edit.raza_activo == true) { edit.raza_activo = 1 }
    else { edit.raza_activo = 0 }
    edit.id = this.item.id;
    let sql = 'UPDATE razas SET raza_nombre = ?, raza_especie = ?, raza_activo=? WHERE id=?';
    this.database.Updatedatabase(sql, edit).then((data) => {
      this.presentAlert();
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.zone.run(() => {
      this.router.navigateByUrl('/mostrar-raza');
    });
  }

}
