import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoRazaPage } from './info-raza.page';

describe('InfoRazaPage', () => {
  let component: InfoRazaPage;
  let fixture: ComponentFixture<InfoRazaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoRazaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoRazaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
