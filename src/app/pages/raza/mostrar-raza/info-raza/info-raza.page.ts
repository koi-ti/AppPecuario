import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-info-raza',
  templateUrl: './info-raza.page.html',
  styleUrls: ['./info-raza.page.scss'],
})
export class InfoRazaPage implements OnInit {
  activatedRoute: any;
  object: any;
  animales: any;

  constructor(
    private database: DatabaseService,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM razas where id = ?";
        this.database.getItemById(data.id, sql).then((datax) => {
          this.object = datax;
          if (this.object.raza_activo == 1) { this.object.raza_activo = 'true'; }
          else { this.object.raza_activo = 'false'; }
        });
        this.Get(data.id);
      })
  }

  Get(raza: number) {
    let sql = "SELECT * FROM animals WHERE animal_raza=?";
    this.database.getItemsById(raza, sql).then((data) => {
      this.animales = data;
    }, (error) => {
      console.log(error);
    });
  }

}
