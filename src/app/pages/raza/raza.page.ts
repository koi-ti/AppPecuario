import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../database.service';
import { ActionSheetController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-raza',
  templateUrl: './raza.page.html',
  styleUrls: ['./raza.page.scss'],
})
export class RazaPage implements OnInit {

  List : any;
  todo : FormGroup;

 constructor(
   private database : DatabaseService,
   private router: Router,
   public formBuilder: FormBuilder,
   public alertController: AlertController,
   private zone: NgZone,
   public actionSheetController: ActionSheetController
   ){

  this.todo = this.formBuilder.group({
    raza_nombre: ['', Validators.required],
    raza_especie: ['', Validators.required],
    raza_activo:true,
  });

 }

 ngOnInit() {
  this.Get(); 
 }

 async presentAlert() {
  const alert = await this.alertController.create({
    // header: 'El Hato ha sido creado con éxito',
    // subHeader: 'Subtitle',
    message: 'La raza ha sido creada',
    buttons: [      
      {text: 'Ok',
      role: 'ok',
      handler: ()=>{
        this.goBack();}
    }]
  });

  await alert.present();
}

 Create(){
  if(this.todo.value.raza_activo==true){
    this.todo.value.raza_activo=1;}
    else{this.todo.value.raza_activo=0;}
  let sql = "INSERT INTO razas (raza_nombre, raza_especie, raza_activo) VALUES(?,?,?)";
  this.database.Createdatabase(this.todo.value, sql).then((data) => {
    }, (error) => {
    console.log(error);
    })
  }

  Get(){
    let sql="SELECT * FROM especies WHERE especie_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.List=data;
    }, (error) => {
      console.log(error);
    })
  }
  
    goBack(){
      this.zone.run(() => {
        this.router.navigateByUrl('/mostrar-raza');
      });
  }
}
