import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-actualizar-animal',
  templateUrl: './actualizar-animal.page.html',
  styleUrls: ['./actualizar-animal.page.scss'],
})
export class ActualizarAnimalPage implements OnInit {

  item: any;
  edit: FormGroup;
  ListHato: any;
  ListItemHato: any;
  ListItemLote: any;
  ListLote: any;
  ListMarca: any;
  ListItemMarca: any;
  foto: any;

  constructor(
    private database: DatabaseService,
    private router: Router,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private camera: Camera,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.GetHato();
    this.GetLote();
    this.GetMarca();
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM animals where id = ?";
        this.database.getItemById(data.id, sql).then(datax => {
          this.item = datax;

          this.item.animal_lote == null ? this.ListItemLote = { id: "" } : this.GetItemLote(this.item.animal_lote);
          this.item.animal_marca == null ? this.ListItemMarca = { id: "" } : this.GetItemMarca(this.item.animal_marca);
          this.item.animal_hato == null ? this.ListItemHato = { id: "" } : this.GetItemHato(this.item.animal_hato);

          if (!this.item) {
            this.goBack();
          }
          else {
            // this.item.animal_activo = this.item.animal_activo == 1 ? true : false;
            if (this.item.animal_activo == 1) { this.item.animal_activo = true }
            else { this.item.animal_activo = false }
            this.edit = this.formBuilder.group({
              animal_numero: new FormControl(this.item.animal_numero),
              animal_activo: new FormControl(this.item.animal_activo),
              // animal_especie: new FormControl(this.item.animal_especie),
              // animal_raza: new FormControl(this.item.animal_raza),
              animal_color: new FormControl(this.item.animal_color),
              animal_peso_nacer: new FormControl(this.item.animal_peso_nacer),
              animal_hato: new FormControl(this.item.animal_hato),
              animal_lote: new FormControl(this.item.animal_lote),
              animal_marca: new FormControl(this.item.animal_marca),
              animal_foto: new FormControl(this.item.animal_foto)
            });
          }
        });
      }
    )
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Atención',
      // subHeader: 'Subtitle',
      message: 'El animal ha sido actualizado',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          cssClass: 'secondary',
          handler: () => {
            this.goBack();
          }
        }
      ]
    });

    await alert.present();
  }

  GetHato() {
    let sql = "SELECT * FROM hatos WHERE hato_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListHato = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetItemHato(lote_hato) {
    let sql = "SELECT * FROM hatos WHERE id=?";
    this.database.getItemById(lote_hato, sql).then((data) => {
      this.ListItemHato = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetLote() {
    let sql = "SELECT * FROM lotes WHERE lote_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListLote = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetItemLote(lote) {
    let sql = "SELECT * FROM lotes WHERE id=?";
    this.database.getItemById(lote, sql).then((data) => {
      this.ListItemLote = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetMarca() {
    let sql = "SELECT * FROM marcas WHERE marca_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListMarca = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetItemMarca(marca) {
    let sql = "SELECT * FROM marcas WHERE id=?";
    this.database.getItemById(marca, sql).then((data) => {
      this.ListItemMarca = data;
    }, (error) => {
      console.log(error);
    })
  }

  Update(edit) {
    // this.item.animal_activo = this.item.animal_activo == true ? 1 : 0;
    if (edit.animal_activo == true) { edit.animal_activo = 1 }
    else { edit.animal_activo = 0 }
    edit.id = this.item.id;
    this.edit.value.animal_foto = this.foto;
    let sql = 'UPDATE animals SET animal_numero = ?, animal_activo=?, animal_color=?, animal_peso_nacer=?, animal_hato=?, animal_lote=?, animal_marca=?, animal_foto=? WHERE id=?';
    this.database.Updatedatabase(sql, edit).then((data) => {
      this.presentAlert();
    }, (error) => {
      console.log(error);
    })
  }

  takePhoto(sourceType: number) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: sourceType,
    }

    this.camera.getPicture(options).then((imageData) => {
      this.foto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  goBack() {
    this.router.navigate(['/mostrar-animal']);
  }

}
