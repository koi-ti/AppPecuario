import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-info-animal',
  templateUrl: './info-animal.page.html',
  styleUrls: ['./info-animal.page.scss'],
})
export class InfoAnimalPage implements OnInit {

  activatedRoute: any;
  object: any;
  Especie: any;
  Raza: any;

  constructor(
    private database: DatabaseService,
    private route: ActivatedRoute,
    private router: Router,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private zone: NgZone,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM animals where id = ?";
        this.database.getItemById(data.id, sql).then((datax) => {
          this.object = datax;          
            this.GetItemEspecie(this.object.animal_especie);
            this.GetItemRaza(this.object.animal_raza);
        });
      })
  }

  GetItemEspecie(animal_especie) {
    let sql = "SELECT * FROM especies WHERE id=?";
    this.database.getItemById(animal_especie, sql).then((data) => {
      this.Especie = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetItemRaza(animal_raza) {
    let sql = "SELECT * FROM razas WHERE id=?";
    this.database.getItemById(animal_raza, sql).then((data) => {
      this.Raza = data;
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.zone.run(() => {
      this.router.navigateByUrl('/mostrar-animal');
    });
  }

}
