import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../database.service';
import { AlertController, Platform, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-mostrar-animal',
  templateUrl: './mostrar-animal.page.html',
  styleUrls: ['./mostrar-animal.page.scss'],
})
export class MostrarAnimalPage implements OnInit {

  List: any;
  ListLote: any;
  todo: FormGroup;
  item: any;
  ListMarca: {};
  ListRaza: {};
  textAnimal: any;

  constructor(
    private database: DatabaseService,
    private router: Router,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public plt: Platform,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.todo = this.formBuilder.group({
      animal_lote: [''],
      animal_raza: [''],
      animal_marca: ['']
    });
  }

  ionViewWillEnter() {
    this.plt.ready().then(() => {
      this.GetLote();
      this.GetRaza();
      this.GetMarca();
    });
  }

  async presentAlert(id) {
    const alert = await this.alertController.create({
      header: 'Atención',
      message: '¿Está seguro que desa borrar el Animal?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log("Cancelar");
        }
      },
      {
        text: 'Ok',
        role: 'ok',
        cssClass: 'secondary',
        handler: () => {
          this.Delete(id);
          this.Get();
        }
      }
      ]
    });

    await alert.present();
  }

  doRefresh(event) {
    setTimeout(() => {
      event.target.complete();
    }, 500);
  }

  Get() {
    this.todo = this.formBuilder.group({
      animal_lote: [''],
      animal_raza: [''],
      animal_marca: ['']
    });
  }

  Delete(id: number) {
    let sql = 'DELETE FROM animals WHERE id=?';
    this.database.Deletedatabase(id, sql).then((data) => {
    }, (error) => {
      console.log(error);
    })
  }

  BuscarAnimal(event) {
    this.textAnimal = event.detail.value;
  }

  GetLote() {
    let sql = "SELECT * FROM lotes";
    this.database.Getdatabase(sql).then((data) => {
      this.ListLote = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetRaza() {
    let sql = "SELECT * FROM razas";
    this.database.Getdatabase(sql).then((data) => {
      this.ListRaza = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetMarca() {
    let sql = "SELECT * FROM marcas";
    this.database.Getdatabase(sql).then((data) => {
      this.ListMarca = data;
    }, (error) => {
      console.log(error);
    })
  }

  Filtro(todo) {
    let object = { sqllote: " animal_lote=?", sqlraza: " AND animal_raza=?", sqlmarca: " AND animal_marca=?" };

    if (this.todo.value.animal_lote == "" && this.todo.value.animal_raza != "" && this.todo.value.animal_marca != "") {
      object.sqllote = "";
      object.sqlraza = " animal_raza=?";
      delete this.todo.value.animal_lote;
    }

    else if (this.todo.value.animal_lote != "" && this.todo.value.animal_raza == "" && this.todo.value.animal_marca != "") {
      object.sqlraza = "";
      delete this.todo.value.animal_raza;
    }

    else if (this.todo.value.animal_lote != "" && this.todo.value.animal_raza != "" && this.todo.value.animal_marca == "") {
      object.sqlmarca = "";
      delete this.todo.value.animal_marca;
    }

    else if (this.todo.value.animal_lote != "" && this.todo.value.animal_raza == "" && this.todo.value.animal_marca == "") {
      object.sqlraza = "";
      object.sqlmarca = "";
      delete this.todo.value.animal_raza;
      delete this.todo.value.animal_marca;
    }

    else if (this.todo.value.animal_lote == "" && this.todo.value.animal_raza != "" && this.todo.value.animal_marca == "") {
      object.sqllote = "";
      object.sqlraza = " animal_raza=?";
      object.sqlmarca = "";
      delete this.todo.value.animal_lote;
      delete this.todo.value.animal_marca;
    }

    else if (this.todo.value.animal_lote == "" && this.todo.value.animal_raza == "" && this.todo.value.animal_marca != "") {
      object.sqllote = "";
      object.sqlraza = "";
      object.sqlmarca = " animal_marca=?";
      delete this.todo.value.animal_lote;
      delete this.todo.value.animal_raza;
    }

    let sql = "SELECT * FROM animals WHERE" + object.sqllote + object.sqlraza + object.sqlmarca;

    this.database.Filtrodb(sql, todo).then((data) => {
      this.List = data;
    }, (error) => {
      console.log(error);
    })
  }

}
