import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarAnimalPage } from './mostrar-animal.page';

describe('MostrarAnimalPage', () => {
  let component: MostrarAnimalPage;
  let fixture: ComponentFixture<MostrarAnimalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarAnimalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarAnimalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
