import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../database.service';
import { AlertController, ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-animal',
  templateUrl: './animal-compra.page.html',
  styleUrls: ['./animal-compra.page.scss'],
})
export class AnimalCompraPage implements OnInit {

  ListEspecie: any;
  ListRaza: any;
  ListHato: any;
  ListLote: any;
  ListMarca: any;
  todo: FormGroup;
  foto: any;
  sql: any;

  constructor(
    private database: DatabaseService,
    private router: Router,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private zone: NgZone,
    private camera: Camera,
    public actionSheetController: ActionSheetController
  ) {


    this.todo = this.formBuilder.group({
      animal_numero: ['', Validators.required],
      animal_especie: ['', Validators.required],
      animal_raza: ['', Validators.required],
      animal_color: [''],
      animal_macho: ['m'],
      animal_ingreso: ['', Validators.required],
      animal_peso_nacer: [''],
      // animal_padre:[''],
      // animal_madre:[''],
      animal_hato: [''],
      animal_lote: [''],
      animal_marca: [''],
      animal_rdfi: [''],
      animal_activo: true,
      animal_observaciones: [''],
      animal_foto: ['']
    });

  }

  ngOnInit() {
    this.GetEspecie();
    this.GetRaza();
    this.GetHato();
    this.GetLote();
    this.GetMarca();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      message: 'El animal ha sido registrado',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          handler: () => {
            this.goBack();
          }
        }]
    });

    await alert.present();
  }

  Create() {
    this.todo.value.animal_activo = true ? 1 : 0;
    this.todo.value.animal_hato = this.todo.value.animal_hato ? this.todo.value.animal_hato : null;
    this.todo.value.animal_lote = this.todo.value.animal_lote ? this.todo.value.animal_lote : null;
    this.todo.value.animal_marca = this.todo.value.animal_marca ? this.todo.value.animal_marca : null;

    this.todo.value.animal_foto = this.foto;

    this.sql = "INSERT INTO animals (animal_numero, animal_especie, animal_raza, animal_color, animal_macho, animal_ingreso, animal_peso_nacer, animal_hato, animal_lote, animal_marca, animal_rdfi, animal_activo, animal_observaciones, animal_foto) VALUES(?,?,?,?,?,datetime(?),?,?,?,?,?,?,?,?)";

    this.database.Createdatabase(this.todo.value, this.sql).then((data) => {

    }, (error) => {
      console.log(error);
    })
  }

  auxiliar() {
    if (this.todo.value.animal_activo == true) {
      this.todo.value.animal_activo = 1;
    }

    else if (this.todo.value.animal_activo == false) {
      this.todo.value.animal_activo = 0;
    }
  }

  GetEspecie() {
    let sql = "SELECT * FROM especies WHERE especie_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListEspecie = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetRaza() {
    let sql = "SELECT * FROM razas WHERE raza_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListRaza = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetHato() {
    let sql = "SELECT * FROM hatos WHERE hato_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListHato = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetLote() {
    let sql = "SELECT * FROM lotes WHERE lote_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListLote = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetMarca() {
    let sql = "SELECT * FROM marcas WHERE marca_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListMarca = data;
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.zone.run(() => {
      this.router.navigateByUrl('/mostrar-animal');
    });
  }

  takePhoto(sourceType: number) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: sourceType,
    }

    this.camera.getPicture(options).then((imageData) => {
      this.foto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

}
