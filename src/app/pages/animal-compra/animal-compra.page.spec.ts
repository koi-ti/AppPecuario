import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalCompraPage } from './animal-compra.page';

describe('AnimalCompraPage', () => {
  let component: AnimalCompraPage;
  let fixture: ComponentFixture<AnimalCompraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimalCompraPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalCompraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
