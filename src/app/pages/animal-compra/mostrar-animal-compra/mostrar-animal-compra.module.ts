import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MostrarAnimalCompraPage } from './mostrar-animal-compra.page';

const routes: Routes = [
  {
    path: '',
    component: MostrarAnimalCompraPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MostrarAnimalCompraPage]
})
export class MostrarAnimalCompraPageModule {}
