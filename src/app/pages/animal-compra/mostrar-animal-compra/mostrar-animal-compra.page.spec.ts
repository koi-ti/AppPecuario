import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarAnimalCompraPage } from './mostrar-animal-compra.page';

describe('MostrarAnimalCompraPage', () => {
  let component: MostrarAnimalCompraPage;
  let fixture: ComponentFixture<MostrarAnimalCompraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarAnimalCompraPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarAnimalCompraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
