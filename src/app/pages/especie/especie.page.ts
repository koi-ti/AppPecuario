import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../database.service';
import { ActionSheetController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-especie',
  templateUrl: './especie.page.html',
  styleUrls: ['./especie.page.scss'],
})
export class EspeciePage implements OnInit {

  List: any;
  todo: FormGroup;

  constructor(
    private database: DatabaseService,
    private router: Router,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private zone: NgZone,
    public actionSheetController: ActionSheetController
  ) {


    this.todo = this.formBuilder.group({
      especie_nombre: ['', Validators.required],
      especie_activo: true,
    });

  }

  ngOnInit() {
    // this.Get();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      message: 'La especie ha sido creada',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          handler: () => {
            this.goBack();
          }
        }]
    });

    await alert.present();
  }

  Create() {
    if (this.todo.value.especie_activo == true) {
      this.todo.value.especie_activo = 1;
    }
    else { this.todo.value.especie_activo = 0; }
    let sql = "INSERT INTO especies (especie_nombre, especie_activo) VALUES(?,?)";
    this.database.Createdatabase(this.todo.value, sql).then((data) => {
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.zone.run(() => {
      this.router.navigateByUrl('/mostrar-especie');
    });
  }

}
