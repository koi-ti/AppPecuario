import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-actualizar-especie',
  templateUrl: './actualizar-especie.page.html',
  styleUrls: ['./actualizar-especie.page.scss'],
})
export class ActualizarEspeciePage implements OnInit {

  edit: FormGroup;
  item: any;

  constructor(
    private database: DatabaseService,
    private router: Router,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM especies where id = ?";
        this.database.getItemById(data.id, sql).then(datax => {
          this.item = datax;
          if (!this.item) {
            this.goBack();
          }
          else {
            if (this.item.especie_activo == 1) { this.item.especie_activo = true }
            else { this.item.especie_activo = false }
            this.edit = this.formBuilder.group({
              especie_nombre: new FormControl(this.item.especie_nombre, Validators.required),
              especie_activo: new FormControl(this.item.especie_activo)
            });
          }
        });
      }
    )
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Atención',
      // subHeader: 'Subtitle',
      message: 'La especie ha sido actualizada',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          cssClass: 'secondary',
          handler: () => {
            this.goBack();
          }
        }
      ]
    });

    await alert.present();
  }

  Update(edit) {
    if (edit.especie_activo == true) { edit.especie_activo = 1 }
    else { edit.especie_activo = 0 }
    edit.id = this.item.id;
    let sql = 'UPDATE especies SET especie_nombre = ?, especie_activo=? WHERE id=?';
    this.database.Updatedatabase(sql, this.edit.value).then((data) => {
      this.presentAlert();
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.router.navigate(['/mostrar-especie']);
  }

}

