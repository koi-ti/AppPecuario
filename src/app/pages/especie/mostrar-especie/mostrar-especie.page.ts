import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DatabaseService } from '../../../database.service';
import { AlertController, Platform } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-mostrar-especie',
  templateUrl: './mostrar-especie.page.html',
  styleUrls: ['./mostrar-especie.page.scss'],
})
export class MostrarEspeciePage implements OnInit {

  List: any;
  todo: FormGroup;
  item: any;
  private object = { respecie: '' };

  constructor(
    private database: DatabaseService,
    private router: Router,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public plt: Platform,
    public actionSheetController: ActionSheetController
  ) {

  }
  ngOnInit() {

  }

  ionViewWillEnter() {
    this.plt.ready().then(() => {
      this.Get();
    });
  }

  async presentAlert(id) {

    if (this.object["count(raza_especie)"] == 0) {

      const alert = await this.alertController.create({
        header: 'Atención',
        // subHeader: 'Subtitle',
        message: '¿Está seguro que desa borrar la Especie?',
        buttons: [{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log("Cancelar");
            this.Get();
          }
        },
        {
          text: 'Ok',
          role: 'ok',
          cssClass: 'secondary',
          handler: () => {
            this.Delete(id);
            this.Get();
          }
        }
        ]
      });

      await alert.present();
    }

    else {
      const alert = await this.alertController.create({
        header: 'Atención',
        // subHeader: 'Subtitle',
        message: 'No es posible borrar la especie ya que existen elementos en su interior',
        buttons: [{
          text: 'Ok',
          role: 'ok',
          handler: () => {
            console.log("Cancelar");
            this.Get();
          }
        }
        ]
      });

      await alert.present();

    }
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 500);
  }

  Get() {
    let sql = "SELECT * FROM especies";
    this.database.Getdatabase(sql).then((data) => {
      this.List = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetRaza(id: number) {
    let sql = "SELECT count(raza_especie) FROM razas WHERE raza_especie=?";
    this.database.getItemById(id, sql).then((data) => {
      this.item = data;
      this.object = this.item;
      // console.log(this.object["count(lote_hato)"]);
      this.presentAlert(id);
    }, (error) => {
      console.log(error);
    })
  }

  Delete(id: number) {
    let sql = 'DELETE FROM especies WHERE id=?';
    this.database.Deletedatabase(id, sql).then((data) => {
    }, (error) => {
      console.log(error);
    })
  }

  goForward() {
    this.router.navigate(['/especie']);
  }

}
