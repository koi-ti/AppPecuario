import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-info-especie',
  templateUrl: './info-especie.page.html',
  styleUrls: ['./info-especie.page.scss'],
})
export class InfoEspeciePage implements OnInit {

  activatedRoute: any;
  object: any;
  razas: any;

  constructor(
    private database: DatabaseService,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM especies where id = ?";
        this.database.getItemById(data.id, sql).then((datax) => {
          this.object = datax;
          if (this.object.especie_activo == 1) { this.object.especie_activo = 'true'; }
          else { this.object.especie_activo = 'false'; }
        });
        this.Get(data.id);
      })
  }

  Get(raza_especie: number) {
    let sql = "SELECT * FROM razas WHERE raza_especie=?";
    this.database.getItemsById(raza_especie, sql).then((data) => {
      this.razas = data;
    }, (error) => {
      console.log(error);
    });
  }
}
