import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoEspeciePage } from './info-especie.page';

describe('InfoEspeciePage', () => {
  let component: InfoEspeciePage;
  let fixture: ComponentFixture<InfoEspeciePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoEspeciePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoEspeciePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
