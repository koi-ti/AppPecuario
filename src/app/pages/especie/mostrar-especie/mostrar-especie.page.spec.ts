import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarEspeciePage } from './mostrar-especie.page';

describe('MostrarEspeciePage', () => {
  let component: MostrarEspeciePage;
  let fixture: ComponentFixture<MostrarEspeciePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarEspeciePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarEspeciePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
