import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../database.service';
import { ActionSheetController, AlertController } from '@ionic/angular';


@Component({
  selector: 'app-marca',
  templateUrl: './marca.page.html',
  styleUrls: ['./marca.page.scss'],
})
export class MarcaPage implements OnInit {

  List: any;
  todo: FormGroup;

  constructor(
    private database: DatabaseService,
    private router: Router,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private zone: NgZone,
    public actionSheetController: ActionSheetController
  ) {


    this.todo = this.formBuilder.group({
      marca_nombre: ['', Validators.required],
      marca_activo: true,
    });

  }

  ngOnInit() {

  }

  async presentAlert() {
    const alert = await this.alertController.create({
      message: 'La marca ha sido creada',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          handler: () => {
            this.goBack();
          }
        }]
    });

    await alert.present();
  }

  Create() {
    if (this.todo.value.marca_activo == true) {
      this.todo.value.marca_activo = 1;
    }
    else { this.todo.value.marca_activo = 0; }
    let sql = "INSERT INTO marcas (marca_nombre, marca_activo) VALUES(?,?)";
    this.database.Createdatabase(this.todo.value, sql).then((data) => {
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.zone.run(() => {
      this.router.navigateByUrl('/mostrar-marca');
    });
  }
}
