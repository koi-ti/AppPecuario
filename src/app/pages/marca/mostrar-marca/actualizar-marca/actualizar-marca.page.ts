import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-actualizar-marca',
  templateUrl: './actualizar-marca.page.html',
  styleUrls: ['./actualizar-marca.page.scss'],
})
export class ActualizarMarcaPage implements OnInit {

  edit: FormGroup;
  item: any;

  constructor(
    private database: DatabaseService,
    private router: Router,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM marcas where id = ?";
        this.database.getItemById(data.id, sql).then(datax => {
          this.item = datax;
          if (!this.item) {
            this.goBack();
          }
          else {
            if (this.item.marca_activo == 1) { this.item.marca_activo = true }
            else { this.item.marca_activo = false }
            this.edit = this.formBuilder.group({
              marca_nombre: new FormControl(this.item.marca_nombre, Validators.required),
              marca_activo: new FormControl(this.item.marca_activo)
            });
          }
        });
      }
    )
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Atención',
      // subHeader: 'Subtitle',
      message: 'La marca ha sido actualizada',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          cssClass: 'secondary',
          handler: () => {
            this.goBack();
          }
        }
      ]
    });

    await alert.present();
  }

  Update(edit) {
    if (edit.marca_activo == true) { edit.marca_activo = 1 }
    else { edit.marca_activo = 0 }
    edit.id = this.item.id;
    let sql = 'UPDATE marcas SET marca_nombre = ?, marca_activo=? WHERE id=?';
    this.database.Updatedatabase(sql, edit).then((data) => {
      this.presentAlert();
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.router.navigate(['/mostrar-marca']);
  }

}
