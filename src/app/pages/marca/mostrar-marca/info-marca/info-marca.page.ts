import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-info-marca',
  templateUrl: './info-marca.page.html',
  styleUrls: ['./info-marca.page.scss'],
})
export class InfoMarcaPage implements OnInit {

  activatedRoute: any;
  object: any;
  animales: any;

  constructor(
    private database: DatabaseService,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM marcas where id = ?";
        this.database.getItemById(data.id, sql).then((datax) => {
          this.object = datax;
          if (this.object.marca_activo == 1) { this.object.marca_activo = 'true'; }
          else { this.object.marca_activo = 'false'; }
        });
        this.Get(data.id);
      })
  }

  Get(marca: number) {
    let sql = "SELECT * FROM animals WHERE animal_marca=?";
    this.database.getItemsById(marca, sql).then((data) => {
      this.animales = data;
    }, (error) => {
      console.log(error);
    });
  }
}