import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoMarcaPage } from './info-marca.page';

describe('InfoMarcaPage', () => {
  let component: InfoMarcaPage;
  let fixture: ComponentFixture<InfoMarcaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoMarcaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoMarcaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
