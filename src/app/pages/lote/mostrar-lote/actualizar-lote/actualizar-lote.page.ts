import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-actualizar-lote',
  templateUrl: './actualizar-lote.page.html',
  styleUrls: ['./actualizar-lote.page.scss'],
})
export class ActualizarLotePage implements OnInit {

  List: any;
  ListHato: any;
  ListItemHato = {};
  edit: FormGroup;
  item: any;

  constructor(
    private database: DatabaseService,
    private router: Router,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private zone: NgZone,
    public actionSheetController: ActionSheetController
  ) {

  }

  ngOnInit() {
    this.GetHato();
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM lotes where id = ?";
        this.database.getItemById(data.id, sql).then(datax => {
          this.item = datax;
          if (!this.item) {
            this.goBack();
          }
          else {
            if (this.item.lote_activo == 1) { this.item.lote_activo = true }
            else { this.item.lote_activo = false }
            this.edit = this.formBuilder.group({
              lote_nombre: new FormControl(this.item.lote_nombre, Validators.required),
              lote_descripcion: new FormControl(this.item.lote_descripcion),
              lote_activo: new FormControl(this.item.lote_activo),
              lote_hato: new FormControl(this.item.lote_hato, Validators.required)
            });
            this.GetItemHato(this.item.lote_hato);
          }
        });
      }
    )
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Atención',
      message: 'El lote ha sido actualizado',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          cssClass: 'secondary',
          handler: () => {
            this.goBack();
          }
        }
      ]
    });

    await alert.present();
  }

  GetHato() {
    let sql = "SELECT * FROM hatos WHERE hato_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.ListHato = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetItemHato(lote_hato) {
    let sql = "SELECT * FROM hatos WHERE id=?";
    this.database.getItemById(lote_hato, sql).then((data) => {
      this.ListItemHato = data;
    }, (error) => {
      console.log(error);
    })
  }

  Update(edit) {
    if (edit.lote_activo == true) { edit.lote_activo = 1 }
    else { edit.lote_activo = 0 }
    edit.id = this.item.id;
    let sql = 'UPDATE lotes SET lote_nombre = ?, lote_descripcion = ?, lote_activo=?, lote_hato=? WHERE id=?';
    this.database.Updatedatabase(sql, edit).then((data) => {
      this.presentAlert();
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.zone.run(() => {
      this.router.navigateByUrl('/mostrar-lote');
    });
  }

}
