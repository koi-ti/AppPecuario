import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, ActionSheetController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-mostrar-lote',
  templateUrl: './mostrar-lote.page.html',
  styleUrls: ['./mostrar-lote.page.scss'],
})
export class MostrarLotePage implements OnInit {

  List: any;
  todo: FormGroup;
  private object = { animal: '' };
  item: any;

  constructor(
    private database: DatabaseService,
    private router: Router,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public plt: Platform,
    public actionSheetController: ActionSheetController
  ) {

  }
  ngOnInit() {

  }

  ionViewWillEnter() {
    this.plt.ready().then(() => {
      this.Get();
    });
  }

  async presentAlert(id) {

    if (this.object["count(animal_lote)"] == 0) {

      const alert = await this.alertController.create({
        header: 'Atención',
        // subHeader: 'Subtitle',
        message: '¿Está seguro que desa borrar el Lote?',
        buttons: [{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log("Cancelar");
            this.Get();
          }
        },
        {
          text: 'Ok',
          role: 'ok',
          cssClass: 'secondary',
          handler: () => {
            this.Delete(id);
            this.Get();
          }
        }
        ]
      });

      await alert.present();
    }

    else {
      const alert = await this.alertController.create({
        header: 'Atención',
        // subHeader: 'Subtitle',
        message: 'No es posible borrar el Lote ya que existen elementos en su interior',
        buttons: [{
          text: 'Ok',
          role: 'ok',
          handler: () => {
            console.log("Cancelar");
            this.Get();
          }
        }
        ]
      });

      await alert.present();

    }
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 500);
  }

  Get() {
    let sql = "SELECT * FROM lotes";
    this.database.Getdatabase(sql).then((data) => {
      this.List = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetAnimal(id: number) {
    let sql = "SELECT count(animal_lote) FROM animals WHERE animal_lote=?";
    this.database.getItemById(id, sql).then((data) => {
      this.item = data;
      this.object = this.item;
      this.presentAlert(id);
    }, (error) => {
      console.log(error);
    })
  }

  Delete(id: number) {
    let sql = 'DELETE FROM lotes WHERE id=?';
    this.database.Deletedatabase(id, sql).then((data) => {
    }, (error) => {
      console.log(error);
    })
  }

  goForward() {
    this.router.navigate(['/lote']);
  }

}
