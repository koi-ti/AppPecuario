import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarLotePage } from './mostrar-lote.page';

describe('MostrarLotePage', () => {
  let component: MostrarLotePage;
  let fixture: ComponentFixture<MostrarLotePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarLotePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarLotePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
