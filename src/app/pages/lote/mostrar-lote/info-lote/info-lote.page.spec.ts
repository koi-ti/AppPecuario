import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoLotePage } from './info-lote.page';

describe('InfoLotePage', () => {
  let component: InfoLotePage;
  let fixture: ComponentFixture<InfoLotePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoLotePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoLotePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
