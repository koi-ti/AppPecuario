import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-info-lote',
  templateUrl: './info-lote.page.html',
  styleUrls: ['./info-lote.page.scss'],
})
export class InfoLotePage implements OnInit {

  activatedRoute: any;
  object: any;
  animales: any;

  constructor(
    private database: DatabaseService,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM lotes where id = ?";
        this.database.getItemById(data.id, sql).then((datax) => {
          this.object = datax;
          if (this.object.lote_activo == 1) { this.object.lote_activo = 'true'; }
          else { this.object.lote_activo = 'false'; }
        });
        this.Get(data.id);
      })
  }

  Get(raza_especie: number) {
    let sql = "SELECT * FROM animals WHERE animal_lote=?";
    this.database.getItemsById(raza_especie, sql).then((data) => {
      this.animales = data;
    }, (error) => {
      console.log(error);
    });
  }

}
