import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../database.service';
import { ActionSheetController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-lote',
  templateUrl: './lote.page.html',
  styleUrls: ['./lote.page.scss'],
})
export class LotePage implements OnInit {

  List: any;
  todo: FormGroup;

  constructor(
    private database: DatabaseService,
    private router: Router,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private zone: NgZone,
    public actionSheetController: ActionSheetController
  ) {

    this.todo = this.formBuilder.group({
      lote_nombre: ['', Validators.required],
      lote_descripcion: [''],
      lote_activo: true,
      lote_hato: ['', Validators.required],

    });

  }

  ngOnInit() {
    this.Get();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      message: 'El lote ha sido creado',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          handler: () => {
            this.goBack();
          }
        }]
    });

    await alert.present();
  }

  Create() {
    if (this.todo.value.lote_activo == true) {
      this.todo.value.lote_activo = 1;
    }
    else { this.todo.value.lote_activo = 0; }
    let sql = "INSERT INTO lotes (lote_nombre, lote_descripcion, lote_activo, lote_hato) VALUES(?,?,?,?)";
    this.database.Createdatabase(this.todo.value, sql).then((data) => {
    }, (error) => {
      console.log(error);
    })
  }

  Get() {
    let sql = "SELECT * FROM hatos WHERE hato_activo=1";
    this.database.Getdatabase(sql).then((data) => {
      this.List = data;
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.zone.run(() => {
      this.router.navigateByUrl('/mostrar-lote');
    });
  }

}
