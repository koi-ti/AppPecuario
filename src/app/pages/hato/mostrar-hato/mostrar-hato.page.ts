import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterModule, Routes } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../database.service';
import { AlertController, Platform, ActionSheetController } from '@ionic/angular';


@Component({
  selector: 'app-mostrar-hato',
  templateUrl: './mostrar-hato.page.html',
  styleUrls: ['./mostrar-hato.page.scss'],
})
export class MostrarHatoPage implements OnInit {

  List: any;
  ListLote: any;
  todo: FormGroup;
  item: any;
  private object = { lhato: '' };


  constructor(
    private database: DatabaseService,
    private router: Router,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public plt: Platform,
    public actionSheetController: ActionSheetController
  ) {

  }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.plt.ready().then(() => {
      this.Get();
    });
  }

  async presentAlert(id) {

    if (this.object["count(lote_hato)"] == 0) {

      const alert = await this.alertController.create({
        header: 'Atención',
        // subHeader: 'Subtitle',
        message: '¿Está seguro que desa borrar el Hato?',
        buttons: [{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log("Cancelar");
            this.Get();
          }
        },
        {
          text: 'Ok',
          role: 'ok',
          cssClass: 'secondary',
          handler: () => {
            this.Delete(id);
            this.Get();
          }
        }
        ]
      });

      await alert.present();
    }

    else {
      const alert = await this.alertController.create({
        header: 'Atención',
        // subHeader: 'Subtitle',
        message: 'No es posible borrar el Hato ya que existen elementos en su interior',
        buttons: [{
          text: 'Ok',
          role: 'ok',
          handler: () => {
            console.log("Cancelar");
            this.Get();
          }
        }
        ]
      });

      await alert.present();

    }
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 500);
  }

  Get() {
    let sql = "SELECT * FROM hatos";
    this.database.Getdatabase(sql).then((data) => {
      this.List = data;
    }, (error) => {
      console.log(error);
    })
  }

  GetLote(id: number) {
    let sql = "SELECT count(lote_hato) FROM lotes WHERE lote_hato=?";
    this.database.getItemById(id, sql).then((data) => {
      this.item = data;
      this.object = this.item;
      // console.log(this.object["count(lote_hato)"]);
      this.presentAlert(id);
    }, (error) => {
      console.log(error);
    })
  }

  Delete(id: number) {
    let sql = 'DELETE FROM hatos WHERE id=?';
    this.database.Deletedatabase(id, sql).then((data) => {
    }, (error) => {
      console.log(error);
    })
  }

  goForward() {
    this.router.navigate(['/hato']);
  }

}
