import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  activatedRoute: any;
  object: any;
  lotes: any;

  constructor(
    private database: DatabaseService,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM hatos where id = ?";
        this.database.getItemById(data.id, sql).then((datax) => {
          this.object = datax;
          if (this.object.hato_activo == 1) { this.object.hato_activo = 'true'; }
          else { this.object.hato_activo = 'false'; }
        });
        this.Get(data.id);
      })
  }

  Get(lote_hato: number) {
    let sql = "SELECT * FROM lotes WHERE lote_hato=?";
    this.database.getItemsById(lote_hato, sql).then((data) => {
      this.lotes = data;
    }, (error) => {
      console.log(error);
    });
  }

}
