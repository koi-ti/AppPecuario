import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../../../database.service';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-actualizar',
  templateUrl: './actualizar.page.html',
  styleUrls: ['./actualizar.page.scss'],
})
export class ActualizarPage implements OnInit {

  edit: FormGroup;
  item: any;

  constructor(
    private database: DatabaseService,
    private router: Router,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private zone: NgZone,
    public actionSheetController: ActionSheetController
  ) {

  }

  ngOnInit() {
    this.route.params.subscribe(
      data => {
        let sql = "SELECT * FROM hatos where id = ?";
        this.database.getItemById(data.id, sql).then(datax => {
          this.item = datax;
          if (!this.item) {
            this.goBack();
          }
          else {
            if (this.item.hato_activo == 1) { this.item.hato_activo = true }
            else { this.item.hato_activo = false }
            this.edit = this.formBuilder.group({
              hato_nombre: new FormControl(this.item.hato_nombre, Validators.required),
              hato_descripcion: new FormControl(this.item.hato_descripcion),
              hato_activo: new FormControl(this.item.hato_activo)
            });
          }
        });
      }
    )
  }



  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Atención',
      // subHeader: 'Subtitle',
      message: 'El hato ha sido actualizado',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          cssClass: 'secondary',
          handler: () => {
            this.goBack();
          }
        }
      ]
    });
    await alert.present();
  }

  Update(edit) {
    if (edit.hato_activo == true) { edit.hato_activo = 1 }
    else { edit.hato_activo = 0 }
    edit.id = this.item.id;
    let sql = 'UPDATE hatos SET hato_nombre = ?, hato_descripcion = ?, hato_activo=? WHERE id=?';
    this.database.Updatedatabase(sql, edit).then((data) => {
      this.presentAlert();
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.zone.run(() => {
      this.router.navigateByUrl('/mostrar-hato');
    });
  }

}

