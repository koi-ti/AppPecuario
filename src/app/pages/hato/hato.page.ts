import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from '../../database.service';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-hato',
  templateUrl: './hato.page.html',
  styleUrls: ['./hato.page.scss'],
})
export class HatoPage implements OnInit {

  List: any;
  todo: FormGroup;
  myphoto: any;

  constructor(
    private database: DatabaseService,
    private router: Router,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private zone: NgZone,
    private camera: Camera,
    public actionSheetController: ActionSheetController
  ) {


    this.todo = this.formBuilder.group({
      hato_nombre: ['', Validators.required],
      hato_descripcion: [''],
      // hato_photo: [''],
      hato_activo: true,
    });

  }

  ngOnInit() {

  }

  async presentAlert() {
    const alert = await this.alertController.create({
      // header: 'El Hato ha sido creado con éxito',
      // subHeader: 'Subtitle',
      message: 'El Hato ha sido creado',
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          handler: () => {
            this.goBack();
          }
        }]
    });

    await alert.present();
  }

  Create() {
    if (this.todo.value.hato_activo == true) {
      this.todo.value.hato_activo = 1;
    } else { 
      this.todo.value.hato_activo = 0; 
    }
    // this.todo.value.hato_activo == true ? 1 : 0;

    let sql = "INSERT INTO hatos (hato_nombre, hato_descripcion, hato_activo) VALUES(?,?,?)";
    this.database.Createdatabase(this.todo.value, sql).then((data) => {
    }, (error) => {
      console.log(error);
    })
  }

  goBack() {
    this.zone.run(() => {
      this.router.navigateByUrl('/mostrar-hato');
    });
    // this.router.navigate(['/mostrar-hato']);
  }

  // takePhoto(sourceType:number) {
  //   const options: CameraOptions = {
  //     quality: 50,
  //     destinationType: this.camera.DestinationType.DATA_URL,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.PICTURE,
  //     correctOrientation: true,
  //     sourceType:sourceType,
  //   }

  //   this.camera.getPicture(options).then((imageData) => {
  //     this.myphoto = 'data:image/jpeg;base64,' + imageData;
  //     this.todo.value.hato_photo = this.myphoto;
  //   }, (err) => {
  //     console.log(err);
  //   });
  // }

}
