import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HatoPage } from './hato.page';

describe('HatoPage', () => {
  let component: HatoPage;
  let fixture: ComponentFixture<HatoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HatoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HatoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
