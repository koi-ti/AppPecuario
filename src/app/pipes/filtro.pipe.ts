import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {
  aux: any;

  transform(arreglo: any, texto: string): any {
    if (texto === '' || texto === undefined) {
      return arreglo;
    }

    else {
      return arreglo.filter(item => {
        return item.animal_numero.includes(texto);
      });
    }

  }

}
