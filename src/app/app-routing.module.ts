import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'hato', loadChildren: './pages/hato/hato.module#HatoPageModule' },
  { path: 'mostrar-hato', loadChildren: './pages/hato/mostrar-hato/mostrar-hato.module#MostrarHatoPageModule' },
  { path: 'lote', loadChildren: './pages/lote/lote.module#LotePageModule' },
  { path: 'especie', loadChildren: './pages/especie/especie.module#EspeciePageModule' },
  { path: 'marca', loadChildren: './pages/marca/marca.module#MarcaPageModule' },
  { path: 'animal', loadChildren: './pages/animal/animal.module#AnimalPageModule' },
  { path: 'mostrar-lote', loadChildren: './pages/lote/mostrar-lote/mostrar-lote.module#MostrarLotePageModule' },
  { path: 'mostrar-especie', loadChildren: './pages/especie/mostrar-especie/mostrar-especie.module#MostrarEspeciePageModule' },
  { path: 'raza', loadChildren: './pages/raza/raza.module#RazaPageModule' },
  { path: 'mostrar-raza', loadChildren: './pages/raza/mostrar-raza/mostrar-raza.module#MostrarRazaPageModule' },
  { path: 'mostrar-marca', loadChildren: './pages/marca/mostrar-marca/mostrar-marca.module#MostrarMarcaPageModule' },
  { path: 'actualizar/:id', loadChildren: './pages/hato/mostrar-hato/actualizar/actualizar.module#ActualizarPageModule' },
  { path: 'mostrar-animal', loadChildren: './pages/animal/mostrar-animal/mostrar-animal.module#MostrarAnimalPageModule' },
  { path: 'animal-compra', loadChildren: './pages/animal-compra/animal-compra.module#AnimalCompraPageModule' },
  { path: 'animal-nacimiento', loadChildren: './pages/animal-nacimiento/animal-nacimiento.module#AnimalNacimientoPageModule' },
  { path: 'actualizar-lote/:id', loadChildren: './pages/lote/mostrar-lote/actualizar-lote/actualizar-lote.module#ActualizarLotePageModule' },
  { path: 'actualizar-especie/:id', loadChildren: './pages/especie/mostrar-especie/actualizar-especie/actualizar-especie.module#ActualizarEspeciePageModule' },
  { path: 'actualizar-raza/:id', loadChildren: './pages/raza/mostrar-raza/actualizar-raza/actualizar-raza.module#ActualizarRazaPageModule' },
  { path: 'actualizar-marca/:id', loadChildren: './pages/marca/mostrar-marca/actualizar-marca/actualizar-marca.module#ActualizarMarcaPageModule' },
  { path: 'actualizar-animal/:id', loadChildren: './pages/animal/mostrar-animal/actualizar-animal/actualizar-animal.module#ActualizarAnimalPageModule' },
  { path: 'actualizar-animal-compra', loadChildren: './pages/animal-compra/mostrar-animal-compra/actualizar-animal-compra/actualizar-animal-compra.module#ActualizarAnimalCompraPageModule' },
  { path: 'mostrar-animal-compra', loadChildren: './pages/animal-compra/mostrar-animal-compra/mostrar-animal-compra.module#MostrarAnimalCompraPageModule' },
  { path: 'info/:id', loadChildren: './pages/hato/mostrar-hato/info/info.module#InfoPageModule' },
  { path: 'info-lote/:id', loadChildren: './pages/lote/mostrar-lote/info-lote/info-lote.module#InfoLotePageModule' },
  { path: 'info-especie/:id', loadChildren: './pages/especie/mostrar-especie/info-especie/info-especie.module#InfoEspeciePageModule' },
  { path: 'info-raza/:id', loadChildren: './pages/raza/mostrar-raza/info-raza/info-raza.module#InfoRazaPageModule' },
  { path: 'info-marca/:id', loadChildren: './pages/marca/mostrar-marca/info-marca/info-marca.module#InfoMarcaPageModule' },
  { path: 'info-animal/:id', loadChildren: './pages/animal/mostrar-animal/info-animal/info-animal.module#InfoAnimalPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
