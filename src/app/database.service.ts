import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  private isOpen: boolean;
  private db: SQLiteObject;
  public i: number;


  constructor(
    private sqlite: SQLite
  ) {
    if (!this.isOpen) {
      // this.sqlite=new SQLite();
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql("CREATE TABLE IF NOT EXISTS hatos(id INTEGER PRIMARY KEY AUTOINCREMENT, hato_nombre TEXT UNIQUE, hato_descripcion TEXT, hato_photo TEXT, hato_activo INTEGER)", []);
        db.executeSql("CREATE TABLE IF NOT EXISTS lotes(id INTEGER PRIMARY KEY AUTOINCREMENT, lote_nombre TEXT  UNIQUE, lote_descripcion TEXT, lote_superficie TEXT, lote_activo INTEGER, lote_hato INTEGER, FOREIGN KEY (lote_hato) REFERENCES hatos(id))", []);
        db.executeSql("CREATE TABLE IF NOT EXISTS especies(id INTEGER PRIMARY KEY AUTOINCREMENT, especie_nombre TEXT  UNIQUE, especie_activo INTEGER)", []);
        db.executeSql("CREATE TABLE IF NOT EXISTS razas(id INTEGER PRIMARY KEY AUTOINCREMENT, raza_nombre TEXT  UNIQUE, raza_especie INTEGER, raza_activo INTEGER, CONSTRAINT fk_especies FOREIGN KEY (raza_especie) REFERENCES especies(id))", []);
        db.executeSql("CREATE TABLE IF NOT EXISTS marcas(id INTEGER PRIMARY KEY AUTOINCREMENT, marca_nombre TEXT  UNIQUE, marca_activo INTEGER)", []);
        db.executeSql("CREATE TABLE IF NOT EXISTS animals(id INTEGER PRIMARY KEY, animal_numero TEXT  UNIQUE, animal_especie INTEGER, animal_raza INTEGER, animal_color TEXT, animal_macho TEXT, animal_ingreso TEXT, animal_peso_nacer INTEGER, animal_padre INTEGER, animal_madre INTEGER,  animal_hato INTEGER, animal_lote INTEGER, animal_marca INTEGER, animal_rdfi TEXT, animal_activo INTEGER, animal_observaciones TEXT, animal_foto TEXT, FOREIGN KEY (animal_padre) REFERENCES animals(id),  FOREIGN KEY (animal_madre) REFERENCES animals(id), FOREIGN KEY (animal_especie) REFERENCES especies(id), FOREIGN KEY (animal_raza) REFERENCES razas(id), FOREIGN KEY (animal_hato) REFERENCES hatos(id), FOREIGN KEY (animal_lote) REFERENCES lotes(id), FOREIGN KEY (animal_marca) REFERENCES marcas(id))", []);
        db.executeSql('PRAGMA foreign_keys = ON', []);
        this.db = db;
        this.isOpen = true;
      }).catch(error => console.log(error));
    }
  }



  Createdatabase(datos: object, sql: string) {
    return new Promise((resolve, reject) => {
      let array = [];

      for (var i in datos) {
        array.push(datos[i]);
      }
      this.db.executeSql(sql, array).then((data) => {
        resolve(data);
      }, (error) => {
        reject(error);
      });
    });
  }

  Getdatabase(sql: string) {
    return new Promise((resolve, reject) => {
      let arrayUsers = [];
      this.db.executeSql(sql, arrayUsers).then((data) => {
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            arrayUsers.push(data.rows.item(i));
          }
        }
        resolve(arrayUsers);
      }, (error) => {
        reject(error);
      });
    });
  }

  getItemById(id: number, sql: string) {
    return new Promise((resolve, reject) => {
      this.db.executeSql(sql, [id]).then((data) => {
        var object = {};
        object = data.rows.item(0);
        resolve(object);
        // console.log(object);
      }, (error) => {
        reject(error);
      });
    });
  }

  getItemsById(id: number, sql: string) {
    return new Promise((resolve, reject) => {
      let arrayItems = [];
      this.db.executeSql(sql, [id]).then((data) => {
        var object = {};
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            object = data.rows.item(i);
            arrayItems.push(object);
          }
        }
        resolve(arrayItems);
      }, (error) => {
        reject(error);
      });
    });
  }

  Deletedatabase(id: number, sql: string) {
    return new Promise((resolve, reject) => {
      this.db.executeSql(sql, [id]).then((data) => {
        resolve(data);
      }, (error) => {
        reject(error);
      });
    });
  }

  Updatedatabase(sql: string, datos: object) {
    return new Promise((resolve, reject) => {
      let array = [];
      for (var i in datos) {
        array.push(datos[i]);
      }
      this.db.executeSql(sql, array).then((data) => {
        resolve(data);
      }, (error) => {
        reject(error);
      });
    });
  }

  Filtrodb(sql: string, datos: object) {
    return new Promise((resolve, reject) => {
      let array = [];
      let array2 = [];
      for (var i in datos) {
        array.push(datos[i]);
      }
      this.db.executeSql(sql, array).then((data) => {
        var object = {};
        for (var i = 0; i < data.rows.length; i++) {
          object = data.rows.item(i);
          array2.push(object);
        }
        resolve(array2);
      }, (error) => {
        reject(error);
      });
    });
  }

}

