import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { timer } from 'rxjs/internal/observable/timer';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Principal',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Hato',
      url: '/mostrar-hato'
    },
    {
      title: 'Lote',
      url: '/mostrar-lote'
    },
    {
      title: 'Especie',
      url: '/mostrar-especie'
    },
    {
      title: 'Raza',
      url: '/mostrar-raza'
    },
    {
      title: 'Marca',
      url: '/mostrar-marca'
    },
    {
      title: 'Animal',
      url: '/mostrar-animal'
    },
  ];

  imagenes = [
    'assets/amanecerllanero.jpg'
  ]

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  showSplash = true; 

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      timer(3000).subscribe(() => this.showSplash = false) // <-- hide animation after 3s
    });
  }
}
